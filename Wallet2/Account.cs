//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wallet2
{
    using System;
    using System.Collections.Generic;
    
    public partial class Account
    {
        public Account()
        {
            this.Incomes = new HashSet<Income>();
            this.Outcomes = new HashSet<Outcome>();
            this.Loans = new HashSet<Loan>();
            this.Transfers = new HashSet<Transfer>();
        }
    
        public int idAccounts { get; set; }
        public int idCurrencies { get; set; }
        public decimal balance { get; set; }
        public string description { get; set; }
        public byte[] image { get; set; }
        public string name { get; set; }
    
        public virtual Currency Currency { get; set; }
        public virtual ICollection<Income> Incomes { get; set; }
        public virtual ICollection<Outcome> Outcomes { get; set; }
        public virtual ICollection<Loan> Loans { get; set; }
        public virtual ICollection<Transfer> Transfers { get; set; }
    }
}
