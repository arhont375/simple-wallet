﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Wallet2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            MessageBox.Show("Произошла ошибка соедниения с базой данных. Проверьте своё подключение. Приложение будет закрыто.");
            foreach (Window win in MainWindow.OwnedWindows)
            {
                win.Close();
            }
            MainWindow.Close();
        }
    }
}
