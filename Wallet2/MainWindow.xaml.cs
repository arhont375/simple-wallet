﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.Serialization;
using System.Data;

using Wallet2.InputForms;
using Wallet2.View;
using Wallet2.OutToDB;

namespace Wallet2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        #region work with mainListView 
        private void mainListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;

            if (mainListView.SelectedItem != null)
            {
                mainDescriptionTextBlock.Text = ((InOutObject)mainListView.SelectedItem).description;
            }
        }
        /// <summary>
        /// Экспортирует в XML текущее содержимое mainListView
        /// </summary>
        private void mainListViewExporButton_Click(object sender, RoutedEventArgs e)
        {
            DataTable data = new DataTable("InOutOperations");

            data.Clear();
            data.Columns.Add("Sum");
            data.Columns.Add("Date");
            data.Columns.Add("Product");
            data.Columns.Add("Category");
            data.Columns.Add("Account");

            foreach (InOutObject item in mainListView.Items)
            {
                DataRow newRow = data.NewRow();
                newRow["Sum"] = item.sum;
                newRow["Date"] = item.date;
                newRow["Product"] = item.productName;
                newRow["Category"] = item.categoryName;
                newRow["Account"] = item.accountName;
                data.Rows.Add(newRow);
            }

            data.WriteXml("data.xml");
            data.WriteXmlSchema("dataSchema.xsd");

            MessageBox.Show("Данныe экспортированы в папку приложения");
        }

        private void deleteInOutButton_Click(object sender, RoutedEventArgs e)
        {
            InOutObject item = mainListView.SelectedItem as InOutObject;

            if (item == null)
            {
                MessageBox.Show("Запись не выбрана");
                return;
            }

            MessageBoxButton messageButton = MessageBoxButton.YesNo;
            string msgString = "Вы действительно хотите удалить запись?";
            string titleString = "Удаление записи";
            MessageBoxResult messageBoxResult = MessageBox.Show(msgString, titleString, messageButton);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                if (item.isIncome)
                {
                    WriteToDB.deleteIncome(item.idOutcome);
                }
                else
                {
                    WriteToDB.deleteOutcome(item.idOutcome);
                }

                mainListView.ItemsSource = (new OutInformation()).getInOutOperation();
            }
        }

        static public AddOutcome AddOutcomeWindow { get; set; }
        private void addOutcomeButton_Click(object sender, RoutedEventArgs e)
        {
            if (AddOutcomeWindow != null)
            {
                return;
            }

            AddOutcomeWindow = new AddOutcome();

            AddOutcomeWindow.Owner = this;
            AddOutcomeWindow.Activate();
            AddOutcomeWindow.Show();
        }

        static public addCurency AddCurrencyWindow { get; set; }
        private void addCurrency_Click(object sender, RoutedEventArgs e)
        {
            if (AddCurrencyWindow != null)
            {
                return;
            }

            AddCurrencyWindow = new addCurency();

            AddCurrencyWindow.Owner = this;
            AddCurrencyWindow.Activate();
            AddCurrencyWindow.Show();
        }
        #endregion
        #region Work with categoryListView

        private void catTreeView_Item_Expanded(object sender, RoutedEventArgs e)
        {
            CategoryTV item = e.OriginalSource as CategoryTV;

            //Значит это продукт
            if (item == null)
            {
                return;
            }

            item.Items.Clear();

            using (var walletDB = new WalletDBEntities())
            {
                //Запрос на получение выборки родительских категорий
                var categoryQuery = (from category in walletDB.Categories
                                     where category.parentCategory == item.idCategories
                                     orderby category.name
                                     select new CategoryTV
                                     {
                                         Header = category.name,
                                         Description = category.description,
                                         idCategories = category.idCategories
                                     }).ToList();

                //Добавление категорий
                foreach (CategoryTV cat in categoryQuery)
                {
                    item.Items.Add(cat);
                }

                var productQuery = (from product in walletDB.Products
                                    where product.idCategories == item.idCategories
                                    orderby product.name
                                    select new ProductTV
                                    {
                                        Header = product.name,
                                        Description = product.description,
                                        ID = product.idProducts,
                                        Image = product.image
                                    }).ToList();

                //Добавление продуктов к выводу
                foreach (ProductTV product in productQuery)
                {
                    item.Items.Add(product);
                }
            }
        }

        object catTreeSelectedItem { get; set; }
        private void catTreeView_Item_Selected(object sender, RoutedEventArgs e)
        {
            catTreeSelectedItem = e.OriginalSource;
            ProductTV item = catTreeSelectedItem as ProductTV;

            if (item != null)
            {
                catDescriptionTextBlock.Text = item.Description;
            }
            else if (catTreeSelectedItem as CategoryTV != null)
            {
                catDescriptionTextBlock.Text = ((CategoryTV) catTreeSelectedItem).Description;
            }
        }
        static public addCategory AddCategoryWindow { get; set; }
        private void addCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            if (AddCategoryWindow != null)
            {
                return;
            }

            AddCategoryWindow = new addCategory();

            AddCategoryWindow.Owner = this;
            AddCategoryWindow.Activate();
            AddCategoryWindow.Show();
        }

        private void updateCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            CategoryTV cat = catTreeSelectedItem as CategoryTV;

            if (cat != null)
            {
                updateCategoryButton.IsEnabled = false;

                if (AddCategoryWindow != null)
                {
                    updateCategoryButton.IsEnabled = true;
                    return;
                }

                AddCategoryWindow = new addCategory();
                AddCategoryWindow.isUpdate = cat.idCategories.Value;

                AddCategoryWindow.Owner = this;
                AddCategoryWindow.Activate();
                AddCategoryWindow.Show();

                updateCategoryButton.IsEnabled = true;
            }
        }
        #endregion
        #region Work with loanListView
        static public addLoan AddLoanWindow { get; set; }
        private void loanAddLoan_Click(object sender, RoutedEventArgs e)
        {
            if (AddLoanWindow != null)
            {
                return;
            }

            AddLoanWindow = new addLoan();

            AddLoanWindow.Owner = this;
            AddLoanWindow.Activate();
            AddLoanWindow.Show();
        }
        private void deleteLoanButton_Click(object sender, RoutedEventArgs e)
        {
            OutLoanObject item = loanListView.SelectedItem as OutLoanObject;

            if (item == null)
            {
                MessageBox.Show("Запись не выбрана");
                return;
            }

            MessageBoxButton messageButton = MessageBoxButton.YesNo;
            string msgString = "Вы действительно хотите удалить запись?";
            string titleString = "Удаление записи";
            MessageBoxResult messageBoxResult = MessageBox.Show(msgString, titleString, messageButton);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                WriteToDB.deleteLoan(item.idLoans);

                loanListView.ItemsSource = (new OutInformation()).getLoans();
            }
        }


        private void loanListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OutLoanObject loan = ((ListView)sender).SelectedItem as OutLoanObject;

            e.Handled = true;

            if (loan == null)
            {
                return;
            }

            loanDescriptionTextBlock.Text = loan.description;
        }

        #endregion
        #region Work with accountListView

        static public AddAccount AddAccountWindow { get; set; }
        private void addAccount_Click(object sender, RoutedEventArgs e)
        {
            if (AddAccountWindow != null)
            {
                return;
            }

            AddAccountWindow = new AddAccount();

            AddAccountWindow.Owner = this;
            AddAccountWindow.Activate();
            AddAccountWindow.Show();
        }

        private void deleteAccount_Click(object sender, RoutedEventArgs e)
        {
            OutAccountObject item = accountListView.SelectedItem as OutAccountObject;

            if (item == null)
            {
                MessageBox.Show("Запись не выбрана");
                return;
            }

            MessageBoxButton messageButton = MessageBoxButton.YesNo;
            string msgString = "Вы действительно хотите удалить счёт? Это удалит все связанные с ним операции и долги";
            string titleString = "Удаление записи";
            MessageBoxResult messageBoxResult = MessageBox.Show(msgString, titleString, messageButton);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                WriteToDB.deleteAccount(item.idAccounts);

                updateAll(Tabs.All);
            }
        }

        private void accountListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
        #endregion

        public enum Tabs { Main, Account, Categories, Loans, All };
        public void updateAll(Tabs tab)
        {
            var ioInf = new OutInformation();
            switch (tab)
            {
                case Tabs.Main:
                    mainListView.ItemsSource = ioInf.getInOutOperation();
                    break;
                case Tabs.Account:
                    accountListView.ItemsSource = ioInf.getAccounts();
                    break;
                case Tabs.Categories:
                    catTreeView.ItemsSource = ioInf.getCategories();
                    break;
                case Tabs.Loans:
                    if (loanListView.ItemsSource == null)
                    {
                        loanListView.Items.Clear();
                    }
                    loanListView.ItemsSource = ioInf.getLoans();
                    break;
                case Tabs.All:
                {
                    mainListView.ItemsSource = ioInf.getInOutOperation();
                    accountListView.ItemsSource = ioInf.getAccounts();
                    catTreeView.ItemsSource = ioInf.getCategories();
                    if (loanListView.ItemsSource == null)
                    {
                        loanListView.Items.Clear();
                    }
                    loanListView.ItemsSource = ioInf.getLoans();
                    break;
                }
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var ioInf = new OutInformation();
            switch (mainTabControl.SelectedIndex)
            {
                case 0 :
                    if (mainListView.ItemsSource == null)
                    { mainListView.ItemsSource = ioInf.getInOutOperation(); }
                    break;
                case 1 :
                    if (accountListView.ItemsSource == null)
                    { accountListView.ItemsSource = ioInf.getAccounts(); }
                    break;
                case 2:
                    if (catTreeView.ItemsSource == null)
                    { catTreeView.ItemsSource = ioInf.getCategories(); }
                    break;
                case 3:
                    if (loanListView.ItemsSource == null)
                    {
                        loanListView.Items.Clear();
                    }
                    loanListView.ItemsSource = ioInf.getLoans();
                    break;
            }
        }

        private void changeAccountButton_Click(object sender, RoutedEventArgs e)
        {
            OutAccountObject account = accountListView.SelectedItem as OutAccountObject;

            if (account != null)
            {
                if (AddAccountWindow != null)
                {
                    return;
                }

                AddAccountWindow = new AddAccount();
                AddAccountWindow.isUpdate = account.idAccounts;

                AddAccountWindow.Owner = this;
                AddAccountWindow.Activate();
                AddAccountWindow.Show();
            }
        }

        private void changeCurrencyButton_Click(object sender, RoutedEventArgs e)
        {
            OutAccountObject account = accountListView.SelectedItem as OutAccountObject;

            if (account != null)
            {
                if (AddCurrencyWindow != null)
                {
                    return;
                }

                AddCurrencyWindow = new addCurency();
                AddCurrencyWindow.isUpdate = account.idCurrencies;

                AddCurrencyWindow.Owner = this;
                AddCurrencyWindow.Activate();
                AddCurrencyWindow.Show();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Проверка на соединение с базой данных
            try
            {
                var walletDB = new WalletDBEntities();
            }
            catch (Exception)
            {
                MessageBox.Show("Проблемы соединения с базой данных, приложение будет закрыто");
                this.Close();
            }
        }
    }
}
