//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wallet2
{
    using System;
    using System.Collections.Generic;
    
    public partial class Outcome
    {
        public Outcome()
        {
            this.Transfers = new HashSet<Transfer>();
        }
    
        public int idOutcome { get; set; }
        public int idAccounts { get; set; }
        public int idProducts { get; set; }
        public decimal sum { get; set; }
        public System.DateTime date { get; set; }
        public byte[] image { get; set; }
        public string description { get; set; }
    
        public virtual Product Product { get; set; }
        public virtual Account Account { get; set; }
        public virtual ICollection<Transfer> Transfers { get; set; }
    }
}
