﻿
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/12/2014 01:18:03
-- Generated from EDMX file: C:\Users\alex\documents\visual studio 2013\Projects\Wallet2\Wallet2\WalletDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [WalletDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Accounts_Currencies]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Accounts] DROP CONSTRAINT [FK_Accounts_Currencies];
GO
IF OBJECT_ID(N'[dbo].[FK_Categories_Categories]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Categories] DROP CONSTRAINT [FK_Categories_Categories];
GO
IF OBJECT_ID(N'[dbo].[FK_Income_Accounts]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Income] DROP CONSTRAINT [FK_Income_Accounts];
GO
IF OBJECT_ID(N'[dbo].[FK_Income_Products]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Income] DROP CONSTRAINT [FK_Income_Products];
GO
IF OBJECT_ID(N'[dbo].[FK_Loans_Accounts]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Loans] DROP CONSTRAINT [FK_Loans_Accounts];
GO
IF OBJECT_ID(N'[dbo].[FK_Outcome_Accounts]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Outcome] DROP CONSTRAINT [FK_Outcome_Accounts];
GO
IF OBJECT_ID(N'[dbo].[FK_Outcome_Products]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Outcome] DROP CONSTRAINT [FK_Outcome_Products];
GO
IF OBJECT_ID(N'[dbo].[FK_Products_Categories]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Products] DROP CONSTRAINT [FK_Products_Categories];
GO
IF OBJECT_ID(N'[dbo].[FK_Transfers_Income]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Transfers] DROP CONSTRAINT [FK_Transfers_Income];
GO
IF OBJECT_ID(N'[dbo].[FK_Transfers_Outcome]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Transfers] DROP CONSTRAINT [FK_Transfers_Outcome];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Accounts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Accounts];
GO
IF OBJECT_ID(N'[dbo].[Categories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Categories];
GO
IF OBJECT_ID(N'[dbo].[Currencies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Currencies];
GO
IF OBJECT_ID(N'[dbo].[Income]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Income];
GO
IF OBJECT_ID(N'[dbo].[Loans]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Loans];
GO
IF OBJECT_ID(N'[dbo].[Outcome]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Outcome];
GO
IF OBJECT_ID(N'[dbo].[Products]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Products];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[Transfers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Transfers];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Categories'
CREATE TABLE [dbo].[Categories] (
    [idCategories] int IDENTITY(0,1) NOT NULL,
    [parentCategory] int  NULL,
    [name] nchar(45)  NOT NULL,
    [description] nchar(45)  NULL,
    [image] varbinary(max)  NULL
);
GO

-- Creating table 'Currencies'
CREATE TABLE [dbo].[Currencies] (
    [idCurrencies] int IDENTITY(0,1) NOT NULL,
    [name] nchar(45)  NOT NULL,
    [shortName] nchar(10)  NOT NULL
);
GO

-- Creating table 'Incomes'
CREATE TABLE [dbo].[Incomes] (
    [idIncome] int IDENTITY(0,1) NOT NULL,
    [idAccounts] int  NOT NULL,
    [idProducts] int  NOT NULL,
    [sum] decimal(19,4)  NOT NULL,
    [date] datetime  NOT NULL,
    [image] varbinary(max)  NULL,
    [description] nchar(45)  NULL
);
GO

-- Creating table 'Outcomes'
CREATE TABLE [dbo].[Outcomes] (
    [idOutcome] int IDENTITY(0,1) NOT NULL,
    [idAccounts] int  NOT NULL,
    [idProducts] int  NOT NULL,
    [sum] decimal(19,4)  NOT NULL,
    [date] datetime  NOT NULL,
    [image] varbinary(max)  NULL,
    [description] nchar(45)  NULL
);
GO

-- Creating table 'Products'
CREATE TABLE [dbo].[Products] (
    [idProducts] int IDENTITY(0,1) NOT NULL,
    [idCategories] int  NOT NULL,
    [name] nchar(45)  NOT NULL,
    [description] nchar(45)  NULL,
    [image] varbinary(max)  NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'Accounts'
CREATE TABLE [dbo].[Accounts] (
    [idAccounts] int IDENTITY(0,1) NOT NULL,
    [idCurrencies] int  NOT NULL,
    [balance] decimal(19,4)  NOT NULL,
    [description] nchar(45)  NULL,
    [image] varbinary(max)  NULL,
    [name] nchar(10)  NULL
);
GO

-- Creating table 'Transfers'
CREATE TABLE [dbo].[Transfers] (
    [idTransfers] int IDENTITY(0,1) NOT NULL,
    [idIncome] int  NOT NULL,
    [idOutcome] int  NOT NULL,
    [sum] decimal(19,4)  NOT NULL,
    [name] nchar(45)  NOT NULL,
    [description] nchar(45)  NULL,
    [image] varbinary(max)  NULL,
    [date] datetime  NULL
);
GO

-- Creating table 'Loans'
CREATE TABLE [dbo].[Loans] (
    [idLoans] int IDENTITY(0,1) NOT NULL,
    [idAccounts] int  NOT NULL,
    [sum] decimal(19,4)  NOT NULL,
    [description] nchar(45)  NULL,
    [name] nchar(45)  NULL,
    [date] datetime  NOT NULL,
    [image] varbinary(max)  NULL,
    [isIaDebtor] bit  NOT NULL
);
GO

-- Creating table 'AccountTransfer'
CREATE TABLE [dbo].[AccountTransfer] (
    [Account_idAccounts] int  NOT NULL,
    [Transfers_idTransfers] int  NOT NULL,
    [Transfers_idIncome] int  NOT NULL,
    [Transfers_idOutcome] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [idCategories] in table 'Categories'
ALTER TABLE [dbo].[Categories]
ADD CONSTRAINT [PK_Categories]
    PRIMARY KEY CLUSTERED ([idCategories] ASC);
GO

-- Creating primary key on [idCurrencies] in table 'Currencies'
ALTER TABLE [dbo].[Currencies]
ADD CONSTRAINT [PK_Currencies]
    PRIMARY KEY CLUSTERED ([idCurrencies] ASC);
GO

-- Creating primary key on [idIncome] in table 'Incomes'
ALTER TABLE [dbo].[Incomes]
ADD CONSTRAINT [PK_Incomes]
    PRIMARY KEY CLUSTERED ([idIncome] ASC);
GO

-- Creating primary key on [idOutcome] in table 'Outcomes'
ALTER TABLE [dbo].[Outcomes]
ADD CONSTRAINT [PK_Outcomes]
    PRIMARY KEY CLUSTERED ([idOutcome] ASC);
GO

-- Creating primary key on [idProducts] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [PK_Products]
    PRIMARY KEY CLUSTERED ([idProducts] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [idAccounts] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [PK_Accounts]
    PRIMARY KEY CLUSTERED ([idAccounts] ASC);
GO

-- Creating primary key on [idTransfers], [idIncome], [idOutcome] in table 'Transfers'
ALTER TABLE [dbo].[Transfers]
ADD CONSTRAINT [PK_Transfers]
    PRIMARY KEY CLUSTERED ([idTransfers], [idIncome], [idOutcome] ASC);
GO

-- Creating primary key on [idLoans] in table 'Loans'
ALTER TABLE [dbo].[Loans]
ADD CONSTRAINT [PK_Loans]
    PRIMARY KEY CLUSTERED ([idLoans] ASC);
GO

-- Creating primary key on [Account_idAccounts], [Transfers_idTransfers], [Transfers_idIncome], [Transfers_idOutcome] in table 'AccountTransfer'
ALTER TABLE [dbo].[AccountTransfer]
ADD CONSTRAINT [PK_AccountTransfer]
    PRIMARY KEY CLUSTERED ([Account_idAccounts], [Transfers_idTransfers], [Transfers_idIncome], [Transfers_idOutcome] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [parentCategory] in table 'Categories'
ALTER TABLE [dbo].[Categories]
ADD CONSTRAINT [FK_Categories_Categories]
    FOREIGN KEY ([parentCategory])
    REFERENCES [dbo].[Categories]
        ([idCategories])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Categories_Categories'
CREATE INDEX [IX_FK_Categories_Categories]
ON [dbo].[Categories]
    ([parentCategory]);
GO

-- Creating foreign key on [idCategories] in table 'Products'
ALTER TABLE [dbo].[Products]
ADD CONSTRAINT [FK_Products_Categories]
    FOREIGN KEY ([idCategories])
    REFERENCES [dbo].[Categories]
        ([idCategories])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Products_Categories'
CREATE INDEX [IX_FK_Products_Categories]
ON [dbo].[Products]
    ([idCategories]);
GO

-- Creating foreign key on [idProducts] in table 'Incomes'
ALTER TABLE [dbo].[Incomes]
ADD CONSTRAINT [FK_Income_Products]
    FOREIGN KEY ([idProducts])
    REFERENCES [dbo].[Products]
        ([idProducts])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Income_Products'
CREATE INDEX [IX_FK_Income_Products]
ON [dbo].[Incomes]
    ([idProducts]);
GO

-- Creating foreign key on [idProducts] in table 'Outcomes'
ALTER TABLE [dbo].[Outcomes]
ADD CONSTRAINT [FK_Outcome_Products]
    FOREIGN KEY ([idProducts])
    REFERENCES [dbo].[Products]
        ([idProducts])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Outcome_Products'
CREATE INDEX [IX_FK_Outcome_Products]
ON [dbo].[Outcomes]
    ([idProducts]);
GO

-- Creating foreign key on [idCurrencies] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [FK_Accounts_Currencies]
    FOREIGN KEY ([idCurrencies])
    REFERENCES [dbo].[Currencies]
        ([idCurrencies])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Accounts_Currencies'
CREATE INDEX [IX_FK_Accounts_Currencies]
ON [dbo].[Accounts]
    ([idCurrencies]);
GO

-- Creating foreign key on [idAccounts] in table 'Incomes'
ALTER TABLE [dbo].[Incomes]
ADD CONSTRAINT [FK_Income_Accounts]
    FOREIGN KEY ([idAccounts])
    REFERENCES [dbo].[Accounts]
        ([idAccounts])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Income_Accounts'
CREATE INDEX [IX_FK_Income_Accounts]
ON [dbo].[Incomes]
    ([idAccounts]);
GO

-- Creating foreign key on [idAccounts] in table 'Outcomes'
ALTER TABLE [dbo].[Outcomes]
ADD CONSTRAINT [FK_Outcome_Accounts]
    FOREIGN KEY ([idAccounts])
    REFERENCES [dbo].[Accounts]
        ([idAccounts])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Outcome_Accounts'
CREATE INDEX [IX_FK_Outcome_Accounts]
ON [dbo].[Outcomes]
    ([idAccounts]);
GO

-- Creating foreign key on [idIncome] in table 'Transfers'
ALTER TABLE [dbo].[Transfers]
ADD CONSTRAINT [FK_Transfers_Income]
    FOREIGN KEY ([idIncome])
    REFERENCES [dbo].[Incomes]
        ([idIncome])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Transfers_Income'
CREATE INDEX [IX_FK_Transfers_Income]
ON [dbo].[Transfers]
    ([idIncome]);
GO

-- Creating foreign key on [idOutcome] in table 'Transfers'
ALTER TABLE [dbo].[Transfers]
ADD CONSTRAINT [FK_Transfers_Outcome]
    FOREIGN KEY ([idOutcome])
    REFERENCES [dbo].[Outcomes]
        ([idOutcome])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Transfers_Outcome'
CREATE INDEX [IX_FK_Transfers_Outcome]
ON [dbo].[Transfers]
    ([idOutcome]);
GO

-- Creating foreign key on [idAccounts] in table 'Loans'
ALTER TABLE [dbo].[Loans]
ADD CONSTRAINT [FK_Loans_Accounts]
    FOREIGN KEY ([idAccounts])
    REFERENCES [dbo].[Accounts]
        ([idAccounts])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Loans_Accounts'
CREATE INDEX [IX_FK_Loans_Accounts]
ON [dbo].[Loans]
    ([idAccounts]);
GO

-- Creating foreign key on [Account_idAccounts] in table 'AccountTransfer'
ALTER TABLE [dbo].[AccountTransfer]
ADD CONSTRAINT [FK_AccountTransfer_Account]
    FOREIGN KEY ([Account_idAccounts])
    REFERENCES [dbo].[Accounts]
        ([idAccounts])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Transfers_idTransfers], [Transfers_idIncome], [Transfers_idOutcome] in table 'AccountTransfer'
ALTER TABLE [dbo].[AccountTransfer]
ADD CONSTRAINT [FK_AccountTransfer_Transfer]
    FOREIGN KEY ([Transfers_idTransfers], [Transfers_idIncome], [Transfers_idOutcome])
    REFERENCES [dbo].[Transfers]
        ([idTransfers], [idIncome], [idOutcome])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountTransfer_Transfer'
CREATE INDEX [IX_FK_AccountTransfer_Transfer]
ON [dbo].[AccountTransfer]
    ([Transfers_idTransfers], [Transfers_idIncome], [Transfers_idOutcome]);
GO

-- Добавить Валюты --
INSERT INTO [dbo].[Currencies]
           ([name]
           ,[shortName])
     VALUES
           ('Рубль'
           ,'р.')
GO


-- Добавить аккаунты --
INSERT INTO [dbo].[Accounts]
           ([idCurrencies]
           ,[balance]
           ,[description]
           ,[image]
           ,[name])
     VALUES
           (0
           ,2000
           ,'Деньги, что присылают родители'
           ,null
           ,'Деньги')
GO

-- Добавить категории --
INSERT INTO [dbo].[Categories] 
           ([parentCategory]
           ,[name]
           ,[description]
           ,[image])
     VALUES
           (null
           ,'Еда'
           ,'Расходы на еду'
           ,null)
GO

INSERT INTO [dbo].[Categories] 
           ([parentCategory]
           ,[name]
           ,[description]
           ,[image])
    VALUES
           (0
           ,'Молочные продукты'
           ,'Расходы на молочные продукты'
           ,null)
GO

INSERT INTO [dbo].[Categories] 
           ([parentCategory]
           ,[name]
           ,[description]
           ,[image])
    VALUES
           (0
           ,'Колбасные изделия'
           , null
           , null)
GO

INSERT INTO [dbo].[Categories] 
           ([parentCategory]
           ,[name]
           ,[description]
           ,[image])
    VALUES
           (0
           ,'Хлебобулочные изделия'
           ,'Расходы на мучные продукты'
           ,null)
GO

-- Добавить продукты--

INSERT INTO [dbo].[Products]
           ([idCategories]
           ,[name]
           ,[description]
           ,[image])
     VALUES
           (1
           ,'Молоко'
           ,null
           ,null)
GO

INSERT INTO [dbo].[Products]
           ([idCategories]
           ,[name]
           ,[description]
           ,[image])
     VALUES
           (1
           ,'Кефир'
           ,null
           ,null)
GO

INSERT INTO [dbo].[Products]
           ([idCategories]
           ,[name]
           ,[description]
           ,[image])
     VALUES
           (2
           ,'Сосиски'
           ,null
           ,null)
GO

INSERT INTO [dbo].[Products]
           ([idCategories]
           ,[name]
           ,[description]
           ,[image])
     VALUES
           (2
           ,'Сырокопчёная колбаса'
           ,null
           ,null)
GO

INSERT INTO [dbo].[Products]
           ([idCategories]
           ,[name]
           ,[description]
           ,[image])
     VALUES
           (3
           ,'Батон'
           ,null
           ,null)
GO

INSERT INTO [dbo].[Products]
           ([idCategories]
           ,[name]
           ,[description]
           ,[image])
     VALUES
           (3
           ,'Хлеб'
           ,null
           ,null)
GO


-- Добавление расходов и доходов -- 

INSERT INTO [dbo].[Outcomes]
           ([idAccounts]
           ,[idProducts]
           ,[sum]
           ,[date]
           ,[image]
           ,[description])
     VALUES
           (0
           ,5
           ,45
           ,'12/12/2014'
           ,null
           ,'Дорогой нынче хлеб')
GO

INSERT INTO [dbo].[Outcomes]
           ([idAccounts]
           ,[idProducts]
           ,[sum]
           ,[date]
           ,[image]
           ,[description])
     VALUES
           (0
           ,1
           ,60
           ,'11/12/2014'
           ,null
           ,'А кефир ещё дороже')
GO

INSERT INTO [dbo].[Incomes]
           ([idAccounts]
           ,[idProducts]
           ,[sum]
           ,[date]
           ,[image]
           ,[description])
     VALUES
           (0
           ,1
           ,100
           ,'12/12/2014'
           ,null
           ,'Перепродал кефир')
GO





-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------