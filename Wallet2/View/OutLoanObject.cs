﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;

namespace Wallet2.View
{
    class OutLoanObject : Loan, IDataErrorInfo
    {
        public string CurrencyName { get; set; }
        public string AccountName { get; set; }

        public OutLoanObject () : base()
        {
            base.name = string.Empty;
        }

        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case "sum" :
                        if (sum <= 0)
                        {
                            return "Сумма должна быть больше нуля";
                        }
                        break;
                    case "name" :
                        if (name.Length == 0 || name.Length > 45)
                        {
                            return "Вы должны ввести имя длинной до 45 символов";
                        }
                        break;
                    case "description" :
                        if (description != null && description.Length > 45)
                        {
                            return "Описание должно быть не более 45 символов";
                        }
                        break;
                }
                return string.Empty;
            }
        }

        string IDataErrorInfo.Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }
}
