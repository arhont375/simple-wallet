﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Wallet2.View
{
    class ProductTV : TreeViewItem
    {
        public ProductTV()
            : base()
        {
        }

        public string Description { get; set; }
        public int ID { get; set; }
        public byte[] Image { get; set; }
    }
}
