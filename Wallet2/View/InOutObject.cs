﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;

namespace Wallet2
{
    class InOutObject : Outcome, IDataErrorInfo
    {
        public InOutObject() : base()
        {
        }

        public string productName { get; set; }
        public string categoryName { get; set; }
        public string accountName { get; set; }
        public string currencyName { get; set; }

        public bool isIncome { get; set; }
        
        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case "sum":
                        if (sum <= 0)
                        {
                            return "Сумма должна быть больше нуля";
                        }
                        break;
                    case "description":
                        if (description != null && description.Length > 45)
                        {
                            return "Описание должно быть не более 45 символов";
                        }
                        break;
                    case "productName":
                        if (productName.Length == 0)
                        {
                            return "Вы должны выбрать продукт";
                        }
                        break;
                }
                return string.Empty;
            }
        }

        string IDataErrorInfo.Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }    
    

    }
}
