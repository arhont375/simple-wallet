﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;

namespace Wallet2.View
{
    public class OutAccountObject : Account, IDataErrorInfo
    {
        public OutAccountObject()
        {
        }

        public Decimal CurrentSum { get; set; }
        public string CurrencyName { get; set; }

        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case "name":
                        if (name.Length > 45 || name.Length == 0)
                        {
                            return "Введите имя длиной не более 45 символов";
                        }
                        break;
                    case "description":
                        if (description.Length > 45)
                        {
                            return "Описание должно быть не более 45 символов";
                        }
                        break;
                }
                return string.Empty;
            }
        }

        string IDataErrorInfo.Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }    
        
    }
}
