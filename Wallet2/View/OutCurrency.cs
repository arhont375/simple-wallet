﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace Wallet2.View
{
    [DataContract]
    class OutCurrency : Currency, IDataErrorInfo
    {
        public OutCurrency() : base()
        {
        }
        [IgnoreDataMember]
        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case "name":
                        if (name.Length > 45 || name.Length == 0)
                        {
                            return "Введите имя длиной не более 45 символов";
                        }
                        break;
                    case "shortName":
                        if (shortName.Length > 45 || shortName.Length == 0)
                        {
                            return "Введите короткое имя длинной до 45 символов";
                        }
                        break;
                }
                return string.Empty;
            }
        }
        [IgnoreDataMember]
        string IDataErrorInfo.Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }    
    }
}
