﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;


namespace Wallet2.View
{
    /// <summary>
    /// Класс необходим для вывода категорий в TreeView
    /// </summary>
    class CategoryTV : TreeViewItem
    {
        public CategoryTV()
            : base()
        {
            ///Добавление звёздочки необходимо для дальнейшей работы
            Items.Add("*");
        }

        public string Description { get; set; }
        public Nullable<int> idCategories { get; set; }
    }
}
