﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Wallet2.View
{
    /// <summary>
    /// Необходим для вывода информации из таблицы, для главных страниц
    /// </summary>
    public class OutInformation
    {
        internal System.Collections.IEnumerable getInOutOperation()
        {
            using (var walletDB = new WalletDBEntities())
            {
                //Запрос на получение выборки для главное страницы
                var outcomeQuery = (from outcome in walletDB.Outcomes
                                    join product in walletDB.Products
                                    on outcome.idProducts equals product.idProducts
                                    join category in walletDB.Categories
                                    on product.idCategories equals category.idCategories
                                    join account in walletDB.Accounts
                                    on outcome.idAccounts equals account.idAccounts
                                    join currency in walletDB.Currencies
                                    on account.idCurrencies equals currency.idCurrencies
                                    select new InOutObject
                                    {
                                        sum = -outcome.sum,
                                        date = outcome.date,
                                        productName = product.name,
                                        categoryName = category.name,
                                        idOutcome = outcome.idOutcome,
                                        accountName = account.name,
                                        description = outcome.description,
                                        currencyName = currency.shortName,
                                        isIncome = false
                                    }).ToList();
                var incomeQuery = (from income in walletDB.Incomes
                                   join product in walletDB.Products
                                   on income.idProducts equals product.idProducts
                                   join category in walletDB.Categories
                                   on product.idCategories equals category.idCategories
                                   join account in walletDB.Accounts
                                   on income.idAccounts equals account.idAccounts
                                   join currency in walletDB.Currencies
                                   on account.idCurrencies equals currency.idCurrencies
                                   select new InOutObject
                                   {
                                       sum = income.sum,
                                       date = income.date,
                                       productName = product.name,
                                       categoryName = category.name,
                                       idOutcome = income.idIncome,
                                       accountName = account.name,
                                       description = income.description,
                                       currencyName = currency.shortName,
                                       isIncome = true
                                   }).ToList();

                List<InOutObject> list = new List<InOutObject>(outcomeQuery);

                list.AddRange(incomeQuery);

                foreach (var obj in list)
                {
                    obj.categoryName = obj.categoryName.TrimEnd(" ".ToCharArray());
                    obj.productName = obj.productName.TrimEnd(" ".ToCharArray());
                    obj.accountName = obj.accountName.TrimEnd(" ".ToCharArray());
                    obj.currencyName = obj.currencyName.TrimEnd(" ".ToCharArray());
                }

                //Сортировка операций по дате
                list.Sort(delegate(InOutObject x, InOutObject y)
                {
                    return -x.date.CompareTo(y.date);
                });

                return list;
            }
        }

        internal System.Collections.IEnumerable getAccounts()
        {
            using (var walletDB = new WalletDBEntities())
            {
                //Запрос на получение выборки счетов
                var accountQuery = (from account in walletDB.Accounts
                                   join currency in walletDB.Currencies
                                   on account.idCurrencies equals currency.idCurrencies
                                   orderby account.name
                                   select new OutAccountObject
                                   {
                                       CurrentSum = account.balance,
                                       balance = account.balance,
                                       CurrencyName = currency.shortName,
                                       name = account.name,
                                       idAccounts = account.idAccounts,
                                       idCurrencies = account.idCurrencies
                                   }).ToList();

                if (accountQuery.Count == 0)
                {
                    return accountQuery;
                }

                //Запрос на получение выборки по отчислениям со счетов
                var outcomeQuery = (from outcome in walletDB.Outcomes
                                    group outcome by outcome.idAccounts into outGroup
                                    select new OutAccountObject
                                    {
                                        idAccounts = outGroup.Key,
                                        CurrentSum = outGroup.Sum(p => p.sum) 
                                    }).ToDictionary(account => account.idAccounts);                            

                //Запрос на получение выборки по начислениями на счета
                var incomeQuery = (from income in walletDB.Incomes
                                    group income by income.idAccounts into outGroup
                                    select new OutAccountObject
                                    {
                                        idAccounts = outGroup.Key,
                                        CurrentSum = outGroup.Sum(p => p.sum) 
                                    }).ToDictionary(account => account.idAccounts);                                      
                //Запрос на получение выборки по долгам для каждого счёта
                var loansQuery = (from loan in walletDB.Loans
                                  group loan by loan.idAccounts into loanGroup
                                  select new OutLoanObject
                                  {
                                      idAccounts = loanGroup.Key,
                                      sum = loanGroup.Sum(p => (p.isIaDebtor ? -p.sum : p.sum))
                                  }).ToDictionary(account => account.idAccounts);

                //Вычитание из каждого счёта произошедших трат и прибавление начислений
                foreach (var account in accountQuery)
                {
                    if (outcomeQuery.ContainsKey(account.idAccounts))
                    {
                        account.CurrentSum -= outcomeQuery[account.idAccounts].CurrentSum;
                    }
                    if (incomeQuery.ContainsKey(account.idAccounts))
                    {
                        account.CurrentSum += incomeQuery[account.idAccounts].CurrentSum;
                    }
                    if (loansQuery.ContainsKey(account.idAccounts))
                    {
                        account.CurrentSum += loansQuery[account.idAccounts].sum;
                    }
                }

                return accountQuery;
            }          
        }

        internal System.Collections.IEnumerable getLoans()
        {
            using (var walletDB = new WalletDBEntities())
            {
                //Запрос на получение выборки долгов
                var loanQuery = (from loan in walletDB.Loans
                                    join account in walletDB.Accounts
                                    on loan.idAccounts equals account.idAccounts
                                    join currency in walletDB.Currencies
                                    on account.idCurrencies equals currency.idCurrencies
                                    orderby loan.date descending
                                    select new OutLoanObject
                                    {
                                        //Если я должник, то деньги добавляются
                                        //Иначе наоборот, так как они у нас уменьшаяются 
                                        sum = loan.isIaDebtor ? -loan.sum : loan.sum,
                                        CurrencyName = currency.shortName,
                                        AccountName = account.name,
                                        date = loan.date,
                                        description = loan.description,
                                        name = loan.name,
                                        idLoans = loan.idLoans
                                    }).ToList();

                ////Сортировка по дате
                //loanQuery.Sort(delegate(OutLoanObject x, OutLoanObject y)
                //{
                //    return -x.Date.CompareTo(y.Date);
                //});

                return loanQuery;
            }
        }

        internal System.Collections.IEnumerable getCategories()
        {
            using (var walletDB = new WalletDBEntities())
            {
                //Запрос на получение выборки родительских категорий
                var categoryQuery = (from category in walletDB.Categories
                                     where category.parentCategory == null
                                     orderby category.name
                                     select new CategoryTV 
                                     {
                                        Header = category.name,
                                        Description = category.description,
                                        idCategories = category.idCategories
                                     }).ToList();

                return categoryQuery;
            }
        }

        //TODO: неясно как получить два объекта по одному запросу
        internal System.Collections.IEnumerable getTransfers()
        {
            using (var walletDB = new WalletDBEntities())
            {
                //Запрос на получение переводов
                var transferQuery = (from transfer in walletDB.Transfers
                                     select transfer).ToList();

                return transferQuery;
            }
        }
    }
}
