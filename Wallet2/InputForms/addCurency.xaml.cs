﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Wallet2.View;
using Wallet2.OutToDB;

namespace Wallet2.InputForms
{
    /// <summary>
    /// Interaction logic for addCurency.xaml
    /// </summary>
    public partial class addCurency : Window
    {
        static OutCurrency currency = new OutCurrency();
        /// <summary>
        /// Если обновляем, то хранит id обновляемого элемента, иначе = -1
        /// </summary>
        public int isUpdate = -1;

        Currency currentCurrency;

        public addCurency()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (nameTextBox.ToolTip as string != null ||
                shortNameTextBox.ToolTip as string != null)
            {
                MessageBox.Show("Исправьте ошибки");
                return;
            }


            if (isUpdate != -1)
            {
                currentCurrency.name = currency.name;
                currentCurrency.shortName = currency.shortName;
                currentCurrency.idCurrencies = currency.idCurrencies;
                WriteToDB.updateCurrency(currentCurrency);
            }
            else
            {
                WriteToDB.addCurrency(new Currency()
                {
                    name = currency.name,
                    shortName = currency.shortName,
                    idCurrencies = currency.idCurrencies
                });
            }

            ((MainWindow)this.Owner).updateAll(MainWindow.Tabs.Account);
            MainWindow.AddCurrencyWindow = null;
            this.Close();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.AddCurrencyWindow = null;
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (isUpdate != -1)
            {
                currentCurrency = WriteToDB.getCurrency(isUpdate);
                currency.name = currentCurrency.name;
                currency.shortName = currentCurrency.shortName;
                currency.idCurrencies = currentCurrency.idCurrencies;
            }
            else
            {
                currency.name = string.Empty;
                currency.shortName = string.Empty;
                currency.idCurrencies = -1;
            }

            nameTextBox.DataContext = currency;
            shortNameTextBox.DataContext = currency;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MainWindow.AddCurrencyWindow = null;
        }
    }
}

