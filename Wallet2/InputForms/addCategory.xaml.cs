﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Wallet2.OutToDB;
using Wallet2.View;

namespace Wallet2.InputForms
{
    /// <summary>
    /// Interaction logic for addCategory.xaml
    /// </summary>
    public partial class addCategory : Window
    {

        static OutCategoryObject category =  new OutCategoryObject();
        /// <summary>
        /// Если обновляем, то хранит id обновляемого элемента, иначе = -1
        /// </summary>
        public int isUpdate = -1;

        public addCategory()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (nameTextBox.ToolTip as string != null ||
                descriptionTextBox.ToolTip as string != null)
            {
                MessageBox.Show("Исправьте ошибки");
                return;
            }
            else if (isAProduct.IsChecked.Value && category.parentCategory == null)
            {
                MessageBox.Show("Вы должны выбрать категорию для продукта");
                return;
            }
            else if (isAProduct.IsChecked.Value && isUpdate != -1)
            {
                MessageBox.Show("Категория не может стать продуктом");
                return;
            }

            if (isAProduct.IsChecked.Value)
            {
                WriteToDB.addProduct(new Product()
                    {
                        name = category.name,
                        idCategories = category.parentCategory.Value,
                        description = category.description
                    });
                this.Close();
                return;
            }

            Category cat = new Category()
                {
                    name = category.name,
                    parentCategory = category.parentCategory,
                    description = category.description
                };

            if (isUpdate == -1)
            {
                WriteToDB.addCategory(cat);
            }
            else
            {
                WriteToDB.updateCategory(cat);
                isUpdate = -1;
            }

            ((MainWindow)this.Owner).updateAll(MainWindow.Tabs.Categories);
            MainWindow.AddCategoryWindow = null;
            this.Close();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            isUpdate = -1;
            MainWindow.AddCategoryWindow = null;
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (isUpdate != -1)
            {
                Category cat = WriteToDB.getCategory(isUpdate);
                category.description = cat.description;
                category.name = cat.name;
                category.parentCategory = cat.parentCategory;
                category.idCategories = cat.idCategories;

                catTreeView.IsEnabled = false;
            }
            else
            {
                category.name = string.Empty;
                category.parentCategoryName = string.Empty;
                category.description = string.Empty;
            }
            catTextBlock.DataContext = category;
            nameTextBox.DataContext = category;
            descriptionTextBox.DataContext = category;

            //Построение дерева
            catTreeView.ItemsSource = new List< CategoryTV>() {(new CategoryTV()
            {
                Header = "Все категории"
            })};
        }

        private void catTreeView_Item_Selected(object sender, RoutedEventArgs e)
        {
            CategoryTV item = e.OriginalSource as CategoryTV;

            //Если это категория, до добавляем её в качестве родительской
            if (item != null)
            {
                category.parentCategoryName = (string)item.Header;
                catTextBlock.Text = category.parentCategoryName;
                category.parentCategory = item.idCategories;
            } 
        }

        private void catTreeView_Item_Expanded(object sender, RoutedEventArgs e)
        {
            CategoryTV item = e.OriginalSource as CategoryTV;

            //Значит это продукт
            if (item == null)
            {
                return;
            }

            item.Items.Clear();

            if (item.idCategories == null)
            {
                var categParent = (new OutInformation()).getCategories();

                //Добавление категорий 
                foreach (CategoryTV cat in categParent)
                {
                    cat.Header = cat.Header.ToString().TrimEnd(" ".ToCharArray());
                    item.Items.Add(cat);
                }

                return;
            }
            using (var walletDB = new WalletDBEntities())
            {
                //Запрос на получение выборки родительских категорий
                var categoryQuery = (from category in walletDB.Categories
                                     where category.parentCategory == item.idCategories
                                     orderby category.name
                                     select new CategoryTV
                                     {
                                         Header = category.name,
                                         Description = category.description,
                                         idCategories = category.idCategories
                                     }).ToList();

                //Добавление категорий 
                foreach (CategoryTV cat in categoryQuery)
                {
                    cat.Header = cat.Header.ToString().TrimEnd(" ".ToCharArray());
                    item.Items.Add(cat);
                }

                var productQuery = (from product in walletDB.Products
                                    where product.idCategories == item.idCategories
                                    orderby product.name
                                    select new ProductTV
                                    {
                                        Header = product.name,
                                        Description = product.description,
                                        ID = product.idProducts,
                                        Image = product.image
                                    }).ToList();

                //Добавление продуктов к выводу
                foreach (ProductTV product in productQuery)
                {
                    product.Header = product.Header.ToString().TrimEnd(" ".ToCharArray());
                    item.Items.Add(product);
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MainWindow.AddCategoryWindow = null;
        }

    }
}
