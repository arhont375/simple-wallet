﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Wallet2.View;
using Wallet2.OutToDB;

using Xceed.Wpf.Toolkit;

namespace Wallet2.InputForms
{
    /// <summary>
    /// Interaction logic for AddOutcome.xaml
    /// </summary>
    public partial class AddOutcome : Window
    {
        static InOutObject outcome = new InOutObject();

        public AddOutcome()
        {
            InitializeComponent();
        }

        private void accComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OutAccountObject item = accComboBox.SelectedItem as OutAccountObject;

            if (item != null)
            {
                outcome.idAccounts = item.idAccounts;
            }
        }

        private void dateTimePicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            outcome.date = dateTimePicker.Value.HasValue ? dateTimePicker.Value.Value : DateTime.Now;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.AddOutcomeWindow = null;
            this.Close();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            // Костыль для проверки валидации, как лучше не знаю
            if (sumTextBox.ToolTip as string != null ||
                prodTextBlock.ToolTip as string != null ||
                descriptionTextBox.ToolTip as string != null                )
            {
                System.Windows.MessageBox.Show("Исправьте все ошибки");
                return;
            }

            if (outcome.isIncome)
            {
                Income _income = new Income()
                {
                    sum = outcome.sum,
                    description = outcome.description,
                    date = outcome.date,
                    idAccounts = outcome.idAccounts,
                    idProducts = outcome.idProducts
                };

                WriteToDB.addIncome(_income);

                ((MainWindow)this.Owner).updateAll(MainWindow.Tabs.Main);
                MainWindow.AddOutcomeWindow = null;
                this.Close();
                return;
            }

            Outcome _outcome = new Outcome()
            {
                sum = outcome.sum,
                description = outcome.description,
                date = outcome.date,
                idAccounts = outcome.idAccounts,
                idProducts = outcome.idProducts
            };

            WriteToDB.addOutcome(_outcome);

            ((MainWindow)this.Owner).updateAll(MainWindow.Tabs.Main);
            MainWindow.AddOutcomeWindow = null;
            this.Close();
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            using (var walletDB = new WalletDBEntities())
            {
                //Таким образом я получаю по LINQ запросу данные в виде списка, которые затем использую
                var accQuery = (from acc in walletDB.Accounts
                                orderby acc.name
                                select new OutAccountObject
                                {
                                    idAccounts = acc.idAccounts,
                                    name = acc.name
                                }).ToList();

                accComboBox.ItemsSource = accQuery;
            }

            if (accComboBox.Items.Count != 0)
            {
                accComboBox.SelectedIndex = 0;
            }
            else
            {
                System.Windows.MessageBox.Show("Сначала создайте счёт");
                MainWindow.AddOutcomeWindow = null;
                this.Close();
            }

            sumTextBox.DataContext = outcome;
            descriptionTextBox.DataContext = outcome;
            outcome.isIncome = false;

            outcome.productName = string.Empty;
            prodTextBlock.DataContext = outcome;

            dateTimePicker.Value = DateTime.Now;

            dateTimePicker.Format = DateTimeFormat.Custom;
            dateTimePicker.FormatString = "d.M.yyyy HH:mm";

            catTreeView.ItemsSource = (new OutInformation()).getCategories();
        }

        private void catTreeView_Item_Selected(object sender, RoutedEventArgs e)
        {
            ProductTV item = e.OriginalSource as ProductTV;

            //Если это проукт
            if (item != null)
            {
                outcome.idProducts = item.ID;
                prodTextBlock.Text = (string) item.Header;
            }
        }

        private void catTreeView_Item_Expanded(object sender, RoutedEventArgs e)
        {
            CategoryTV item = e.OriginalSource as CategoryTV;

            //Значит это продукт
            if (item == null)
            {
                return;
            }

            item.Items.Clear();

            using (var walletDB = new WalletDBEntities())
            {
                //Запрос на получение выборки родительских категорий
                var categoryQuery = (from category in walletDB.Categories
                                     where category.parentCategory == item.idCategories
                                     orderby category.name
                                     select new CategoryTV
                                     {
                                         Header = category.name,
                                         Description = category.description,
                                         idCategories = category.idCategories
                                     }).ToList();

                //Добавление категорий 
                foreach (CategoryTV cat in categoryQuery)
                {
                    item.Items.Add(cat);
                }

                var productQuery = (from product in walletDB.Products
                                    where product.idCategories == item.idCategories
                                    orderby product.name
                                    select new ProductTV
                                    {
                                        Header = product.name,
                                        Description = product.description,
                                        ID = product.idProducts,
                                        Image = product.image
                                    }).ToList();

                //Добавление продуктов к выводу
                foreach (ProductTV product in productQuery)
                {
                    item.Items.Add(product);
                }
            }
        }

        private void isIncome_Checked(object sender, RoutedEventArgs e)
        {
            outcome.isIncome = isIncome.IsChecked.Value;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MainWindow.AddOutcomeWindow = null;
        }

    }
}
