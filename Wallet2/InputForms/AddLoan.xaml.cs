﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Wallet2.View;
using Wallet2.OutToDB;

using Xceed.Wpf.Toolkit;

namespace Wallet2.InputForms
{
    /// <summary>
    /// Interaction logic for addLoan.xaml
    /// </summary>
    public partial class addLoan : Window
    {
        static OutLoanObject loan = new OutLoanObject();

        public addLoan()
        {
            InitializeComponent();            
        }

        private void addLoan1_Loaded(object sender, RoutedEventArgs e)
        {
            using (var walletDB = new WalletDBEntities())
            {
                //Таким образом я получаю по LINQ запросу данные в виде списка, которые затем использую
                var accQuery = (from acc in walletDB.Accounts
                                       orderby acc.name
                                       select new OutAccountObject{
                                           idAccounts = acc.idAccounts,
                                           name = acc.name
                                       }).ToList();

                accComboBox.ItemsSource = accQuery;
            }

            if (accComboBox.Items.Count != 0)
            {
                accComboBox.SelectedIndex = 0;
            }
            else
            {
                System.Windows.MessageBox.Show("Сначала создайте аккаунт");
            }
            
            dateTimePicker.Value = DateTime.Now;

            dateTimePicker.Format = DateTimeFormat.Custom;
            dateTimePicker.FormatString = "d.M.yyyy HH:mm";

            descriptionTextBox.DataContext = loan;
            nameTextBox.DataContext = loan;
            sumTextBox.DataContext = loan;
        }

        private void accComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            loan.idAccounts = ((OutAccountObject) accComboBox.SelectedItem).idAccounts;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.AddLoanWindow = null;
            this.Close();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            //Проверка, что валидация не прошла успешно
            if (nameTextBox.ToolTip as string != null || 
                sumTextBox.ToolTip as string != null || 
                descriptionTextBox.ToolTip as string != null)
            {
                System.Windows.MessageBox.Show("Вы должны исправить ошибки");
                return;
            }

            Loan _loan = new Loan()
                {
                    sum = loan.sum,
                    description = loan.description,
                    isIaDebtor = loan.isIaDebtor,
                    idAccounts = loan.idAccounts,
                    date = loan.date,
                    name = loan.name,
                };

            WriteToDB.addLoan(_loan);

            ((MainWindow)this.Owner).updateAll(MainWindow.Tabs.All);
            MainWindow.AddLoanWindow = null;
            this.Close();
        }

        private void youPayCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            loan.isIaDebtor = youPayCheckBox.IsChecked.Value;
;
        }


        private void dateTimePicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            loan.date = dateTimePicker.Value.HasValue ? dateTimePicker.Value.Value : DateTime.Now;
        }

        private void addLoan1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MainWindow.AddLoanWindow = null;
        }
    }
}
