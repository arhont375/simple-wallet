﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Wallet2.View;
using Wallet2.OutToDB;

namespace Wallet2.InputForms
{
    /// <summary>
    /// Interaction logic for AddAccount.xaml
    /// </summary>
    public partial class AddAccount : Window
    {
        static OutAccountObject account = new OutAccountObject();
        /// <summary>
        /// Если обновляем, то хранит id обновляемого элемента, иначе = -1
        /// </summary>
        public int isUpdate = -1;
        public AddAccount()
        {
            InitializeComponent();
        }


        private void curComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            account.idCurrencies = ((OutCurrency)curComboBox.SelectedItem).idCurrencies;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            using (var walletDB = new WalletDBEntities())
            {
                //Таким образом я получаю по LINQ запросу данные в виде списка, которые затем использую
                var curQuery = (from cur in walletDB.Currencies
                                            orderby cur.name
                                            select new OutCurrency
                                            {
                                                idCurrencies = cur.idCurrencies,
                                                name = cur.name
                                            }).ToList();

                foreach (var cur in curQuery)
                {
                    cur.name = cur.name.TrimEnd(" ".ToCharArray());
                }

                curComboBox.ItemsSource = curQuery;
            }

            if (curComboBox.Items.Count == 0)
            {
                MessageBox.Show("Добавьте сначала валюту");
                MainWindow.AddAccountWindow = null;
                this.Close();
                return;
            }

            if (isUpdate != -1)
            {
                Account acc = WriteToDB.getAccount(isUpdate);
                account.balance = acc.balance;
                account.description = acc.description;
                account.idAccounts = acc.idCurrencies;
                account.idCurrencies = acc.idCurrencies;
                account.name = acc.name;
                foreach (OutCurrency cur in curComboBox.Items)
                {
                    if ( cur.idCurrencies == account.idCurrencies )
                    {
                        curComboBox.SelectedItem = cur;
                        break;
                    }
                }
            }
            else
            {
                account.idAccounts = 0;
                account.description = string.Empty;
                account.name = string.Empty;
                account.balance = 0;
                curComboBox.SelectedIndex = 0;
            }
            descriptionTextBox.DataContext = account;
            nameTextBox.DataContext = account;
            sumTextBox.DataContext = account;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.AddAccountWindow = null;
            this.Close();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (descriptionTextBox.ToolTip as string != null ||
                nameTextBox.ToolTip as string != null ||
                sumTextBox.ToolTip as string != null)
            {
                MessageBox.Show("Исправьте ошибки");
                return;
            }

            Account acc = new Account()
            {
                balance = account.balance,
                name = account.name,
                description = account.description,
                idCurrencies = account.idCurrencies,
                idAccounts = account.idAccounts
            };

            if (isUpdate == -1)
            {
                WriteToDB.addAccount(acc);
            }
            else
            {
                WriteToDB.updateAccount(acc);
            }

            ((MainWindow)this.Owner).updateAll(MainWindow.Tabs.Account);
            MainWindow.AddAccountWindow = null;
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MainWindow.AddAccountWindow = null;
        }
    }
}
