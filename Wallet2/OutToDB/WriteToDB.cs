﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Objects;

using System.Data.EntityClient;

namespace Wallet2.OutToDB
{
    static class WriteToDB
    {
        public static void addLoan (Loan loan)
        {
            using (var walletDB = new WalletDBEntities())
            {
                loan.Account = walletDB.Accounts.Where(acc => acc.idAccounts == loan.idAccounts).First();
                walletDB.Loans.Add(loan);
                walletDB.SaveChanges();
            }
        }

        public static void addOutcome(Outcome outcome)
        {
            using (var walletDB = new WalletDBEntities())
            {
                outcome.Product = walletDB.Products.Where(prod => prod.idProducts == outcome.idProducts).First();
                outcome.Account = walletDB.Accounts.Where(acc => acc.idAccounts == outcome.idAccounts).First();
                walletDB.Outcomes.Add(outcome);
                walletDB.SaveChanges();
            }
        }
        public static void addIncome(Income income)
        {
            using (var walletDB = new WalletDBEntities())
            {
                income.Product = walletDB.Products.Where(prod => prod.idProducts == income.idProducts).First();
                income.Account = walletDB.Accounts.Where(acc => acc.idAccounts == income.idAccounts).First();
                walletDB.Incomes.Add(income);
                walletDB.SaveChanges();
            }
        }
        public static void addAccount(Account account)
        {
            using (var walletDB = new WalletDBEntities())
            {
                account.Currency = walletDB.Currencies.Where(cur => cur.idCurrencies == account.idCurrencies).First();
                walletDB.Accounts.Add(account);
                walletDB.SaveChanges();
            }
        }
        public static void addCategory(Category category)
        {
            using (var walletDB = new WalletDBEntities())
            {
                if (category.parentCategory != null)
                {
                    category.Category1 = walletDB.Categories.Where(cat => cat.idCategories == category.idCategories).First();
                }
                walletDB.Categories.Add(category);
                walletDB.SaveChanges();
            }
        }
        public static void addProduct(Product product)
        {
            using (var walletDB = new WalletDBEntities())
            {
                product.Category = walletDB.Categories.Where(cat => cat.idCategories == product.idCategories).First();
                walletDB.Products.Add(product);
                walletDB.SaveChanges();
            }
        }
        public static void addCurrency(Currency currency)
        {
            using (var walletDB = new WalletDBEntities())
            {
                walletDB.Currencies.Add(currency);
                walletDB.SaveChanges();
            }
        }
        public static void deleteOutcome(int id)
        {
            using (var walletDB = new WalletDBEntities())
            {
                Outcome outcome = walletDB.Outcomes.Find(id);
                walletDB.Outcomes.Remove(outcome);
                walletDB.SaveChanges();
            }
        }


        internal static void deleteIncome(int id)
        {
            using (var walletDB = new WalletDBEntities())
            {
                Income income = walletDB.Incomes.Find(id);
                walletDB.Incomes.Remove(income);
                walletDB.SaveChanges();
            }
        }

        internal static void deleteLoan(int id)
        {
            using (var walletDB = new WalletDBEntities())
            {
                Loan loan = walletDB.Loans.Find(id);
                walletDB.Loans.Remove(loan);
                walletDB.SaveChanges();
            }
        }

        internal static void deleteAccount(int id)
        {
            using (var walletDB = new WalletDBEntities())
            {
                //Выбрать переводы связанные с этим аккаунтом
                //var transfers = (from transfer in walletDB.Transfers
                //                 where (transfer.Account.ElementAt(0).idAccounts ==  id || transfer.Account.ElementAt(1).idAccounts == id) 
                //                 select transfer);
                
                //walletDB.Transfers.RemoveRange(transfers);

                

                //Выбрать долги связанные с этим аккаунтом
                var loans = (from loan in walletDB.Loans
                             where (loan.idAccounts == id)
                             select loan);
                walletDB.Loans.RemoveRange(loans);
                //Выбрать расходы и доходы
                var outcomes = (from outcome in walletDB.Outcomes
                                where (outcome.idAccounts == id)
                                select outcome);
                walletDB.Outcomes.RemoveRange(outcomes);
                var incomes = (from income in walletDB.Outcomes
                               where (income.idAccounts == id)
                               select income);
                walletDB.Outcomes.RemoveRange(incomes);
                //Удаление этого счёта
                walletDB.Accounts.Remove(walletDB.Accounts.Find(id));
                //Сохранение сделанных измененеий
                walletDB.SaveChanges();
            }
        }

        internal static Category getCategory(int id)
        {
            using (var walletDB = new WalletDBEntities())
            {
                return walletDB.Categories.Where(cat => cat.idCategories == id).First();
            }
        }

        internal static void updateCategory(Category categ)
        {
            using (var walletDB = new WalletDBEntities())
            {
                Category category = walletDB.Categories.Where(cat => cat.idCategories == categ.idCategories).First();

                category.name = categ.name;
                category.description = categ.description;
                category.image = categ.image;

                walletDB.Entry(category).Property(cat => cat.name).IsModified = true;
                walletDB.Entry(category).Property(cat => cat.description).IsModified = true;
                walletDB.Entry(category).Property(cat => cat.image).IsModified = true;

                walletDB.SaveChanges();
            }
        }

        internal static Account getAccount(int id)
        {
            using (var walletDB = new WalletDBEntities())
            {
                return walletDB.Accounts.Where(acc => acc.idAccounts == id).First();
            }
        }

        internal static void updateAccount(Account acc)
        {
            using (var walletDB = new WalletDBEntities())
            {
                Account account = walletDB.Accounts.Where(a => a.idAccounts == acc.idAccounts).First();

                account.idCurrencies = acc.idCurrencies;
                account.name = acc.name;
                account.description = acc.description;
                account.balance = acc.balance;

                walletDB.Entry(account).Property(a => a.name).IsModified = true;
                walletDB.Entry(account).Property(a => a.description).IsModified = true;
                walletDB.Entry(account).Property(a => a.balance).IsModified = true;
                walletDB.Entry(account).Property(a => a.idCurrencies).IsModified = true;

                walletDB.SaveChanges();
            }
        }

        internal static Currency getCurrency(int id)
        {
            using (var walletDB = new WalletDBEntities())
            {
                return walletDB.Currencies.Where(cur => cur.idCurrencies == id).First();
            }
        }

        internal static void updateCurrency(Currency currency)
        {
            using (var walletDB = new WalletDBEntities())
            {
                Currency cur = walletDB.Currencies.Where(c => c.idCurrencies == currency.idCurrencies).First();

                cur.name = currency.name;
                cur.shortName = currency.shortName;

                walletDB.Entry(cur).Property(c => c.name).IsModified = true;
                walletDB.Entry(cur).Property(c => c.shortName).IsModified = true;

                walletDB.SaveChanges();
            }
        }
    }
}
